import java.io.Console;

import javafx.scene.paint.Color;

public class GameLogic {
	
	public GameLogic(XOUltimateXOs ultimate_board) {
		this.ultimate_board = ultimate_board;
	}

	private int checkRows(int board[][]) {
		for (int i = 0; i < 3; ++i) {
			 if (board[i][0] + board[i][1] + board[i][2] == 3) 
		          return XPIECE;
			 else if (board[i][0] + board[i][1] + board[i][2] == -3)
				 return OPIECE;
			}
		return EMPTY;
	}
	
	private int checkColumns(int board[][]) {
		for (int i = 0; i < 3; ++i) {
			 if (board[0][i] + board[1][i] + board[2][i] == 3) 
		          return XPIECE;
			 else if (board[0][i] + board[1][i] + board[2][i] == -3)
				 return OPIECE;
			}
		return EMPTY;
	}
	
	
	private int checkDiagonals(int board[][]) {
		if (board[0][0] + board[1][1] + board[2][2] == 3) 
	          return XPIECE;
		else if (board[0][0] + board[1][1] + board[2][2] == -3)
			 return OPIECE;
		if (board[0][2] + board[1][1] + board[2][0] == 3) 
	          return XPIECE;
		else if (board[0][2] + board[1][1] + board[2][0] == -3)
			 return OPIECE;
		return EMPTY;
	}
	
	public int InspectSingleBoard(int board[][]) {
		if (checkRows(board) == XPIECE || checkColumns(board) == XPIECE || checkDiagonals(board) == XPIECE)
			return XPIECE;
		else if (checkRows(board) == OPIECE || checkColumns(board) == OPIECE || checkDiagonals(board) == OPIECE)
			return OPIECE;
		return EMPTY;
	}
	
	public void setMirrorPlayables(XOBoard mirror_playable_board, XOBoard ultimate[][]) {
		for (int y = 0; y < 3; ++y)
			for (int x = 0; x < 3; ++x) {
				ultimate[y][x].setMirrorPlayable(false);
			}
		mirror_playable_board.setMirrorPlayable(true);
	}

	public void showMirrorPlayables(XOBoard ultimate[][], int current_player) {
		for (int y = 0; y < 3; ++y)
			for (int x = 0; x < 3; ++x) {
				if (ultimate[y][x].getMirrorPlayable() == true) {
					if (current_player == XPIECE)
						ultimate[y][x].changeGridColour(Color.GREEN);
					else if (current_player == OPIECE)
						ultimate[y][x].changeGridColour(Color.RED);
				}
				else
					ultimate[y][x].changeGridColour(Color.WHITE);	
			}
	}
	
	//Board reference
	XOUltimateXOs ultimate_board;
	
	//Board constants
	private final int EMPTY = 0;
	private final int XPIECE = 1;
	private final int OPIECE = -1;
}
