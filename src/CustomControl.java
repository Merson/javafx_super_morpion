import javafx.event.EventHandler;
import javafx.scene.control.Control;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

public class CustomControl extends Control {

	public CustomControl() {
		
		setSkin(new CustomControlSkin(this));
		ultimatexoboard = new XOUltimateXOs();
		getChildren().add(ultimatexoboard);
	
		setOnMouseClicked(new EventHandler<MouseEvent>() {
			
			@Override
			public void handle(MouseEvent event) {
				ultimatexoboard.placePiece(event.getX(), event.getY());
			}
			
		});
		
		setOnKeyPressed(new EventHandler<KeyEvent>() {
			
			public void handle(KeyEvent event) {
				if (event.getCode() == KeyCode.SPACE)
					ultimatexoboard.resetGame();
			}
			
		});
	}

	@Override
	public void resize(double width, double height) {
		super.resize(width, height);
		ultimatexoboard.resize(width, height);
	}
	private XOUltimateXOs ultimatexoboard;
}
