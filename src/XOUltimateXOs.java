import javax.swing.GroupLayout.Alignment;

import javafx.geometry.Pos;
import javafx.scene.control.Cell;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.transform.Translate;

public class XOUltimateXOs extends Pane {

	public XOUltimateXOs() {
		ultimateBoard = new int[3][3];
		ultimateRenders = new XOBoard[3][3];
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++) {
				ultimateBoard[i][j] = EMPTY;
				ultimateRenders[i][j] = new XOBoard(this);
				getChildren().add(ultimateRenders[i][j]);
			}
		setGrid();
		current_player = XPIECE;
		ultimateLogic = new GameLogic(this);
	}

	public void  setGrid() {
		back = new Rectangle();
		back.setFill(Color.WHITE);
		h1 = new Line(); h2 = new Line();
		v1 = new Line(); v2 = new Line();
		h1.setStroke(Color.GOLD); h2.setStroke(Color.GOLD);
		v1.setStroke(Color.GOLD); v2.setStroke(Color.GOLD);
		h1.setStartX(0); h1.setStartY(0); h1.setEndY(0);
		h2.setStartX(0); h2.setStartY(0); h2.setEndY(0);
		v1.setStartX(0); v1.setStartY(0); v1.setEndX(0);
		v2.setStartX(0); v2.setStartY(0); v2.setEndX(0);
		ch_one = new Translate(0, 0); ch_two = new Translate(0, 0);
		h1.getTransforms().add(ch_one); h2.getTransforms().add(ch_two);
		cw_one = new Translate(0, 0); cw_two = new Translate(0, 0);
		v1.getTransforms().add(cw_one); v2.getTransforms().add(cw_two);
		getChildren().addAll(back, h1, h2, v1, v2);	
	}
	
	@Override
	public void resize(double width, double height) {
		super.resize(width, height);
		cell_width = width / 3.0; cell_height = height / 3.0;
		ch_one.setY(cell_height); ch_two.setY(2 * cell_height);
		h1.setEndX(width); h2.setEndX(width);
		cw_one.setX(cell_width); cw_two.setX(2 * cell_width);
		v1.setEndY(height); v2.setEndY(height);
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				ultimateRenders[i][j].relocate(i * cell_width, j * cell_height);
				ultimateRenders[i][j].resize(cell_width, cell_height);
			}
		}
	}

	public void resetGame() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				ultimateBoard[i][j] = 0;
				getChildren().remove(ultimateRenders[i][j]);
				ultimateRenders[i][j] = new XOBoard(this);
				getChildren().add(ultimateRenders[i][j]);
			}
		}
		setGrid();
	}
	
	public int getCurrentPlayer() {
		return this.current_player;
	}

	public void setCurrentPlayer(int current_player) {
		this.current_player = current_player;
	}

	public void placePiece(final double x, final double y) {
		int indexx = (int) (x / cell_width);
		int indexy = (int) (y / cell_height);
		
		if (ultimateRenders[indexx][indexy].getMirrorPlayable()) {
			ultimateRenders[indexx][indexy].placePiece(x % cell_width, y % cell_height);
			//System.out.println(x % cell_width + "  " +  y % cell_height);
			System.out.println((x % cell_width) % cell_width + "  " +  (y % cell_height) % cell_height);
			checkSingleBoard(indexx, indexy);
			checkUltimateBoard();
		}
	}
	
	private void checkSingleBoard(final double x, final double y) {
		int indexx = (int)x;
		int indexy = (int)y;
		int check = ultimateLogic.InspectSingleBoard(ultimateRenders[indexx][indexy].getBoard());
		if (check == XPIECE) {
			ultimateBoard[indexx][indexy] = 1;
			ultimateRenders[indexx][indexy].setBack(Color.DARKRED);
			ultimateRenders[indexx][indexy].setPlayable(false);
		}
		else if (check == OPIECE) {
			ultimateBoard[indexx][indexy] = -1;
			ultimateRenders[indexx][indexy].setBack(Color.DARKGREEN);
			ultimateRenders[indexx][indexy].setPlayable(false);
		}
	}
	
	private void checkUltimateBoard() {
		int check = ultimateLogic.InspectSingleBoard(ultimateBoard);
		if (check == XPIECE)
			showWinner(Color.RED, "RED X");
		else if (check == OPIECE)
			showWinner(Color.GREEN, "GREEN O");
	}
	
	private void showWinner(Color color, String winner) {
		//resetGame();
		for (int y = 0; y < 3; ++y)
			for (int x = 0; x < 3; ++x) {
				//ultimateRenders[x][y].setBack(color);
				ultimateRenders[x][y].setPlayable(false);
				Text text = new Text(10, 20, winner + " has won the game !");
				text.setFill(Color.WHITE);
				text.setFont(Font.font("Verdana", 20));
				getChildren().add(text);
			}
	}
	
	public XOBoard[][] getUltimateRenders() {
		return this.ultimateRenders;
	}
	
	public double getCellWidth() {
		return this.cell_width;
	}
	
	public double getCellHeight() {
		return this.cell_height;
	}
	
	public GameLogic getGameLogic() {
		return this.ultimateLogic;
	}
	
	//Board logics
	private int[][] ultimateBoard;
	private XOBoard[][] ultimateRenders;
	public GameLogic ultimateLogic;
	
	//Board renders
	private Rectangle back;
	private Line h1, h2, v1, v2;
	private Translate ch_one, ch_two, cw_one, cw_two;
	private double cell_width, cell_height;

	private int current_player;
	
	//Board constants
	private final int EMPTY = 0;
	private final int XPIECE = 1;
	private final int OPIECE = -1;
}
